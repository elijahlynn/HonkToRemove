Website for the #HonkToRemove movement, https://www.honktoremove.info/. 

Pull requests welcome. 

This will soon be a feed aggregator of all the #HonkToRemove videos from social networks. Current plan is to build it in GatsbyJS and will soon include instruction on how to participate, along with printing your own signs. 

